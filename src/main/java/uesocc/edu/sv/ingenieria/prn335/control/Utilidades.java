package uesocc.edu.sv.ingenieria.prn335.control;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import uesocc.edu.sv.ingenieria.prn335.entities.TipoVehiculo;

@EJB
@Stateless
public class Utilidades {

    @PersistenceContext(unitName = "uesocc.edu.sv.ingenieria.prn335_guia05_war_1.0-SNAPSHOTPU")
    private EntityManager managerE;

    /**
     * Metodo para hacer una consulta general de la tabla Marca
     *
     * @return retorna una lista con los valores de la tabla
     * @throws java.lang.Exception
     */
    public List selectAll() throws Exception {
        Query query = managerE.createQuery("SELECT o FROM TipoVehiculo o");
        return query.getResultList();
    }

    /**
     * Metodo para buscar un parametro en la tabla TipoVehiculo
     *
     * @param busqueda recive un criterio de busqueda
     * @return retorna todas los campos que contengan el criterio
     * @throws java.lang.Exception
     */
    public List findByCampos(String busqueda) throws Exception {
        Query query = managerE.createQuery("select o from TipoVehiculo o where (o.activo ='" + busqueda + "' or o.cantidadEjes='" + busqueda + "' or o.descripcion='" + busqueda + "' or o.idTipoVehiculo='" + busqueda + "' or o.nombre='" + busqueda + "' or o.pesoMaxLbs='" + busqueda + "' or o.pesoMinLbs='" + busqueda + "')");
        return query.getResultList();
    }

    /**
     * Metodo para consultar un rango de datos tomando como referencia el id
     *
     * @param inicio recive un valor inicial
     * @param fin recive un valor final
     * @return retorna una lista con la consulta de los rangos
     * @throws java.lang.Exception
     */
    public List findRange(int inicio, int fin) throws Exception {
        Query query = managerE.createQuery("select o from TipoVehiculo o WHERE o.idMarca BETWEEN '" + inicio + "' AND '" + fin + "'");
        return query.getResultList();
    }

    /**
     * Este metodo inserta registros en la tabla TipoVehiculo
     *
     * @param entity recive una entidad con los nuevos datos
     * @throws Exception puede lanzar alguna exception
     */
    public void insert(TipoVehiculo entity) throws Exception {
        managerE.persist(entity);
    }

    /**
     * este metodo borra registros de la tabla TipoVehiculo
     *
     * @param entity recive el valor a borrar
     * @throws Exception puede lanzar alguna exception
     */
    public void delete(TipoVehiculo entity) throws Exception {
        managerE.remove(entity);
    }

    /**
     *Este metodo actualiza un registro de la tabla TipoVehiculo
     * @param entity recive el valor a actualizar
     * @throws Exception puede lanzar alguna exception
     */
    public void update(TipoVehiculo entity) throws Exception {
        managerE.merge(entity);
    }

}
